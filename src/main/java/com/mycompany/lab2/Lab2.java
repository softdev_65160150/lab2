/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

/**
 *
 * @author DELL
 */
import java.util.Scanner;
public class Lab2 {
    
    static char currentPlayer = 'X';
    static int row,col;
    
    static void printWelcome(){
        System.out.println("Welcome OX");
    }
    
    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};

    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    
    static void printTurn(){
        System.out.println(currentPlayer+" Turn");
    }
    
    static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input row col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if(table[row-1][col-1]=='-'){
               table[row-1][col-1] = currentPlayer;
                break;
            }
        }
    }
    
    static boolean isWin(){
        if(checkRow()||checkCol()||checkX1()|checkX2()){
            return true;
        }
        return false;
    }
    
    static boolean checkRow(){
        for(int i=0; i<3; i++){
            if(table[row-1][i]!=currentPlayer){
                return false;
            } 
        }
        return true;
    }

    
    static boolean checkCol(){
        for(int i=0;i<3;i++){
            if(table[i][col-1]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
    
    static boolean checkX1(){
        for(int i=0;i<3;i++){
            if(table[i][i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
    
    static boolean checkX2(){
        for(int i=0;i<3;i++){
            if(table[i][2-i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

    
    static boolean isDraw(){
        return checkDraw();
    }
    
    
    static boolean checkDraw(){
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            if(table[i][j]=='-'){
                return false;
            }
        }
    }
    return true;
}
    
    static void switchPlayer(){
        if(currentPlayer=='X'){
            currentPlayer='O';
        }else{
            currentPlayer = 'X';
        }
    }
    
    static void printWin(){
        System.out.println(currentPlayer+" Win!!");        
    }
    
    static void printDraw(){
        System.out.println("Draw!!");
    }
    
    static boolean inputContinue(){
        Scanner sc = new Scanner(System.in);
        System.out.print("continue(y/n) : ");
        char isContinue = sc.next().charAt(0);
        if(isContinue=='y'){
            return true;
        }
        return false;
    }
    
    static void setTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
               table[i][j]= '-';
            }
        }
    }

    
 
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            while (true) {
                printTable();
                printTurn();
                inputRowCol();
                if (isWin()) {
                    printTable();
                    printWin();
                    break;
                }
                if (isDraw()) {
                    printTable();
                    printDraw();
                    break;
                }
                switchPlayer();

            }
            if (inputContinue()) {
                setTable();
            } else {
                break;
            }
        }
    }
}